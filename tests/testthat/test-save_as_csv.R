# WARNING - Generated by {fusen} from /dev/flat_study_squirrels.Rmd: do not edit by hand

test_that("save_as_csv works", {
  expect_true(inherits(save_as_csv, "function"))

  mytempfile <- tempfile(pattern = "saveascsv")

  dir.create(mytempfile)

  save_as_csv(
    data = data_act_squirrels,
    out_file = file.path(mytempfile, "output.csv")
  )
  
  expect_true(file.exists(file.path(mytempfile, "output.csv")))
  
  expect_equal(
    save_as_csv(
      data = data_act_squirrels,
      out_file = file.path(mytempfile, "output.csv")
    ),
    file.path(mytempfile, "output.csv")
  )
  
  # expect_equal(
  #   data_act_squirrels,
  #   read.csv2(file.path(mytempfile, "output.csv"))
  # )
  
  
  expect_error(
    save_as_csv(
      data = 3,
      out_file =  file.path(mytempfile, "output.csv")
    ),
    "data is not a data frame"
  )
  
  expect_error(
    save_as_csv(
      data = data_act_squirrels,
      out_file =  file.path(mytempfile, "output.xlsx")
    ),
    "File 'output.xlsx' does not have extension csv"
  )
})
