## code to prepare `DATASET` dataset goes here
library(dplyr)

data_act_squirrels <- readr::read_csv(file = "data-raw/nyc_squirrels_act_sample.csv")

data_act_squirrels <- data_act_squirrels %>%
  slice_sample(n = 15)

usethis::use_data(data_act_squirrels, overwrite = TRUE)

cat(sinew::makeOxygen("data_act_squirrels"),
    file = "R/doc_data_act_squirrels.R")
rstudioapi::navigateToFile("R/doc_data_act_squirrels.R")

attachment::att_amend_desc()
