
<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrelspascal

<!-- badges: start -->
<!-- badges: end -->

The goal of squirrelspascal is to …

## Installation

You can install the development version of squirrelspascal like so:

``` r
# FILL THIS IN! HOW CAN PEOPLE INSTALL YOUR DEV PACKAGE?
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(squirrelspascal)
## basic example code
```

Super package sur les ecureuils

``` r
get_message_fur_color(primary_fur_color = "black")
#> We will focus on black squirrels
```
