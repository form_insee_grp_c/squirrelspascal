#' @title DATASET_TITLE
#' @description DATASET_DESCRIPTION
#' @format A data frame with 15 rows and 4 variables:
#' \describe{
#'   \item{\code{age}}{character COLUMN_DESCRIPTION}
#'   \item{\code{primary_fur_color}}{character COLUMN_DESCRIPTION}
#'   \item{\code{activity}}{character COLUMN_DESCRIPTION}
#'   \item{\code{counts}}{double COLUMN_DESCRIPTION} 
#'}
#' @details DETAILS
"data_act_squirrels"